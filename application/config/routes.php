<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'member/auth/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['register'] = 'member/auth/register';
$route['dashboard'] = 'member/page/dashboard';