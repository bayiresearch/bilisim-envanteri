
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Bilişim Envanteri Beta</title>
  <meta name="description" content="Bilişim Envanteri" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <!-- <link rel="apple-touch-icon" href="../assets/images/logo.svg"> -->
  <meta name="apple-mobile-web-app-title" content="Bilişim Envanteri">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <!-- <link rel="shortcut icon" sizes="196x196" href="../assets/images/logo.svg"> -->
  
  <!-- style -->

  <link rel="stylesheet" href="<?php echo base_url('public/libs/font-awesome/css/font-awesome.min.css');?>" type="text/css" />

  <!-- build:css ../assets/css/app.min.css -->
  <link rel="stylesheet" href="<?php echo base_url('public/libs/bootstrap/dist/css/bootstrap.min.css');?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url('public/assets/css/app.css');?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url('public/assets/css/style.css');?>" type="text/css" />
  <!-- endbuild -->
</head>
<body>


<div class="app" id="app">

<!-- ############ LAYOUT START-->

	<?php $this->load->view('modules/login_main');?>

  <?php $this->load->view('modules/login_aside'); ?>

<!-- ############ LAYOUT END-->
</div>


<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
  <script src="<?php echo base_url('public/libs/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
  <script src="<?php echo base_url('public/libs/popper.js/dist/umd/popper.min.js');?>"></script>
  <script src="<?php echo base_url('public/libs/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<!-- core -->
  <script src="<?php echo base_url('public/libs/pace-progress/pace.min.js');?>"></script>
  <script src="<?php echo base_url('public/libs/pjax/pjax.js');?>"></script>

  <script src="<?php echo base_url('public/scripts/lazyload.config.js');?>"></script>
  <script src="<?php echo base_url('public/scripts/lazyload.js');?>"></script>
  <script src="<?php echo base_url('public/scripts/plugin.js');?>"></script>
  <script src="<?php echo base_url('public/scripts/nav.js');?>"></script>
  <script src="<?php echo base_url('public/scripts/scrollto.js');?>"></script>
  <script src="<?php echo base_url('public/scripts/toggleclass.js');?>"></script>
  <script src="<?php echo base_url('public/scripts/theme.js');?>"></script>
  <script src="<?php echo base_url('public/scripts/ajax.js');?>"></script>
  <script src="<?php echo base_url('public/scripts/app.js');?>"></script>
<!-- endbuild -->
</body>
</html>
