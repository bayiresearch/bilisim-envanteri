<!-- ############ Content START-->
<div id="content" class="app-content box-shadow-0 login" role="main">
<!-- Main -->
	<div class="content-main " id="content-main">
		<!-- ############ Main START-->
		<div>
				<div class="row">
					<div class="col-md-8 ml-auto mr-auto login_header">
						<h2>Teknolojik İşbirliği Portalı</h2>
						<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consectetur, quae. Soluta voluptas, minus cumque blanditiis ex asperiores molestias magni sapiente harum quos dolores vitae praesentium quas totam reiciendis voluptatem dolorum?</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 ml-auto mr-auto login_stats">
						<div class="row">
							<div class="col-md-4">
								<div class="box list-item"><div class="list-body"><h4 class="m-0 text-md"><a href="#">+1000 <br> <span class="text-sm">Yazılım Firması</span></a></h4></div></div>
							</div>
							<div class="col-md-4">
								<div class="box list-item"><div class="list-body"><h4 class="m-0 text-md"><a href="#">+400 <br> <span class="text-sm">Donanım Firması</span></a></h4></div></div>
							</div>
							<div class="col-md-4">
								<div class="box list-item"><div class="list-body"><h4 class="m-0 text-md"><a href="#">+50 <br><span class="text-sm">Kategori</span></a></h4></div></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8 ml-auto mr-auto">
						<div class="row">
							<div class="col-md-12">
								<ul class="list-group box">
								<li class="list-group-item">
										<a href="#">
											<span class="d-block _500">Ellipsis description</span>
										</a>
										<span class="clear text-ellipsis text-muted">Vestibulum ullamcorper sodales nisi nec condimentum Aliquam sollicitudin venenatis ipsum</span>
									</li>
									<li class="list-group-item">
										<a href="#">
											<span class="d-block _500">Ellipsis description</span>
										</a>
										<span class="clear text-ellipsis text-muted">Vestibulum ullamcorper sodales nisi nec condimentum Aliquam sollicitudin venenatis ipsum</span>
									</li>
									<li class="list-group-item">
										<a href="#">
											<span class="d-block _500">Ellipsis description</span>
										</a>
										<span class="clear text-ellipsis text-muted">Vestibulum ullamcorper sodales nisi nec condimentum Aliquam sollicitudin venenatis ipsum</span>
									</li>
									<li class="list-group-item">
										<a href="#">
											<span class="d-block _500">Ellipsis description</span>
										</a>
										<span class="clear text-ellipsis text-muted">Vestibulum ullamcorper sodales nisi nec condimentum Aliquam sollicitudin venenatis ipsum</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
		</div>
		<!-- ############ Main END-->
	</div>
</div>
<!-- ############ Content END-->