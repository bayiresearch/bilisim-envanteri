<!-- ############ Aside START-->
<div id="aside" class="app-aside fade box-shadow-x nav-expand white" aria-hidden="true">
		<div class="sidenav white">
		  <!-- sidenav top -->
		  <div class="navbar white">
		    <!-- brand -->
		    <a href="<?php echo base_url();?>" class="navbar-brand">
		    	<img src="<?php echo base_url('public/assets/images/tekport-logo.png');?>" alt="Bilişim Envanteri" class="logo">
		    </a>
		    <!-- / brand -->
		  </div>
		
		  <!-- Flex nav content -->
		  <div class="flex hide-scroll white">
		      <div class="scroll">
		        <div class="nav-border b-primary" data-nav>
							<h4 class="login_slogan">İzmir Ticaret Odası<br>Teknolojik İşbirliği Portalı</h4>
							<div class="box login_form">
								<div class="box-body">
										<form>
											<div class="form-group"><input type="email" class="form-control" id="eposta" placeholder="Eposta Adresiniz"></div>
											<div class="form-group"><input type="password" class="form-control" id="sifre" placeholder="Şifre"></div>
											<a href="<?php echo base_url('dashboard');?>" class="btn default">Giriş</a> <a href="<?php echo base_url('register');?>" class="btn primary">Kayıt Ol</a>
										</form>
								</div>
							</div>
							<div class="box social_media">
								<div class="box-body">
										<ul class="login_social">
											<li><a href=""><i class="fa fa-facebook"></i></a></li>
											<li><a href=""><i class="fa fa-twitter"></i></a></li>
											<li><a href=""><i class="fa fa-linkedin"></i></a></li>
										</ul>
								</div>
							</div>
		        </div>
		      </div>
		  </div>
		  
		</div>
</div>
<!-- ############ Aside END-->