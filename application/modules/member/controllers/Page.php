<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

    /**
     * Member dashboard
     */
	public function dashboard()
	{
		$this->load->view('pages/dashboard');
    }
    
}
