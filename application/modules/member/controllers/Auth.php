<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    /**
     * Member login page
     */
	public function login()
	{
		$this->load->view('pages/login');
    }
    
    /**
     * Member logout page
     */
    public function logout()
    {
        
    }

    /**
     * Member register page
     */
    public function register()
    {
        $this->load->view('pages/register');
    }

    /**
     * Forget password page
     */
    public function forget_password()
    {
        $this->load->view('pages/forget_password');
    }
}
